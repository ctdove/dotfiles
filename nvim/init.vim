" for GNU screen support
if match($TERM, "screen")!=-1
	" set terminal=xterm
	let g:GNU_Screen_used = 1
	" set splitbelow
endif
" for splitting windows
set splitbelow
set splitright

set exrc
set secure
set nocompatible              " be iMproved, required
filetype off                  " required
filetype plugin on
set number

" set nofoldenable    " disable folding

" change hjkl to ljk;
noremap ; l
noremap l h
" disable Arrow keys in Normal mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>

" Disable Arrow keys in Insert mode
" imap <up> <nop>
" imap <down> <nop>
" imap <left> <nop>
" imap <right> <nop>

set tabstop=2 shiftwidth=2 expandtab

au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)

" add include paths
set path+=/usr/avr/include

" uses F2 to save in insert mode
inoremap <F2> <C-\><C-o>:w<CR>

syntax enable
colorscheme monokai

call plug#begin()
" Language/Syntax Support
Plug 'vim-pandoc/vim-pandoc'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'vim-pandoc/vim-rmarkdown'
Plug 'jalvesaq/Nvim-R', {'branch': 'stable'}
Plug 'rhysd/vim-grammarous'

Plug 'rust-lang/rust.vim'
Plug 'stephpy/vim-yaml'
Plug 'racer-rust/vim-racer'
Plug 'WolfgangMehner/c-support'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'jaxbot/semantic-highlight.vim'
Plug 'nottwo/avr-vim'
Plug 'chrisbra/csv.vim'
Plug 'othree/yajs.vim'
Plug 'othree/es.next.syntax.vim'
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
Plug 'cpcsdk/vim-z80-democoding'
Plug 'wsdjeg/vim-assembly'
Plug 'ARM9/arm-syntax-vim'
Plug 'moll/vim-node'

" Plug 'frazrepo/vim-rainbow'
Plug 'junegunn/rainbow_parentheses.vim'
Plug 'mattn/emmet-vim'

" Formatting
Plug 'tpope/vim-surround'
Plug 'rhysd/vim-clang-format'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'jiangmiao/auto-pairs'

" Visual
Plug 'itchyny/lightline.vim' |
					\ Plug 'mengelbrecht/lightline-bufferline'
Plug 'terryma/vim-multiple-cursors'
Plug 'scrooloose/nerdtree' |
          \ Plug 'Xuyuanp/nerdtree-git-plugin' |
					\ Plug 'scrooloose/nerdcommenter' |
					\ Plug 'PhilRunninger/nerdtree-visual-selection'
Plug 'romkatv/powerlevel10k'
Plug 'ryanoasis/vim-devicons'

" All of your Plugins must be added before the following line
call plug#end()            " required
filetype plugin indent on    " required

" Pretty self explanatory
set pastetoggle=<F3>

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
map <C-S-n> :NERDTreeToggle<CR>
" NerdTree Gitconfig
let NERDTreeShowHidden = 1
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
								\ }

let g:lightline = {
      \ 'colorscheme': 'wombat',
			\ 'active': {
      \   'left': [ [ 'mode', 'paste'  ], [ 'readonly', 'filename', 'modified'  ]  ]
      \ },
			\ 'tabline': {
			\   'left': [ ['buffers']  ],
      \   'right': [ ['close']  ]
      \ },
			\ 'component_expand': {
      \   'buffers': 'lightline#bufferline#buffers'
      \ },
			\ 'component_type': {
      \   'buffers': 'tabsel'
      \ },
			\ 'separator': {
			\ 	'left': '', 'right': ''
  		\ },
			\ 'subseparator': {
			\   'left': '', 'right': '' 
  		\ }
			\ }
set showtabline=2
set guioptions=e
" set showcmd
let &statusline .= "%{line2byte('$') + len(getline('$'))}"

" Commands to switch between files
nnoremap \[ :bp<CR>
nnoremap [\ :bp<CR>

nnoremap \] :bn<CR>
nnoremap ]\ :bn<CR>

" Bail out if something that ran earlier, e.g. a system wide vimrc, does not
" want Vim to use these default values.
if exists('skip_defaults_vim')
  finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
if &compatible
  set nocompatible
endif

" When the +eval feature is missing, the set command above will be skipped.
" Use a trick to reset compatible only when the +eval feature is missing.
silent! while 0
set nocompatible
silent! endwhile

" Formatting Rules
set tabstop=2 
set shiftwidth=2
set textwidth=80
autocmd FileType python set tabstop=4|set shiftwidth=2|set expandtab

let g:clang_enable_format_command = 1 
let g:clang_format#style_options = { 
            \ "Standard" : "C++11",
            \ "IndentWidth" : 2,
            \ "UseTab" : "false",
            \ "AccessModifierOffset" : -2,
            \ "ColumnLimit" : 0 }

" Allow backspacing over everything in insert mode.
set backspace=indent,eol,start

set history=200		" keep 200 lines of command line history
set ruler		" show the cursor position all the time
" set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line

set ttimeout		" time out for key codes
set ttimeoutlen=100	" wait up to 100ms after Esc for special key

" Show @@@ in the last line if it is truncated.
set display=truncate
" for lightline
set laststatus=2
set noshowmode

" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Do incremental searching when it's possible to timeout.
if has('reltime')
  set incsearch
endif

" Do not recognize octal numbers for Ctrl-A and Ctrl-X, most users find it
" confusing.
set nrformats-=octal

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries.
if has('win32')
  set guioptions-=t
endif

" Don't use Ex mode, use Q for formatting.
" Revert with ":unmap Q".
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" Revert with ":iunmap <C-U>".
inoremap <C-U> <C-G>u<C-U>

" Enables mouse support in visual mode
if has('mouse')
  if &term =~ 'xterm'
    set mouse=a
  else
    set mouse=nvi
  endif
endif

" Only do this part when Vim was compiled with the +eval feature.
if 1

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  " Revert with ":filetype off".
  filetype plugin indent on

  " Put these in an autocmd group, so that you can revert them with:
  " ":augroup vimStartup | au! | augroup END"
  augroup vimStartup
    au!

    " When editing a file, always jump to the last known cursor position
		autocmd BufReadPost *
      \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
      \ |   exe "normal! g`\""
      \ | endif
  augroup END

endif

" Switch syntax highlighting on when the terminal has colors or when using the
" GUI (which always has colors).
if &t_Co > 2 || has("gui_running")
  " Revert with ":syntax off".
  syntax on

  " highlights strings inside C comments, undo w/ :unlet c_comment_strings".
  let c_comment_strings=1
endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
" Revert with: ":delcommand DiffOrig".
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif




" for Coc Autocomplete ========================================================

let g:coc_global_extensions = [
   \ 'coc-json', 'coc-ccls', 'coc-highlight', 'coc-tsserver', 'coc-prettier',
   \ 'coc-r-lsp', 'coc-go', 'coc-texlab', 'coc-pyright'
\ ]

" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
" set cmdheight=2
set cmdheight=1

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" Custom key mappings
" Shorcut for adding comments
" noremap <buffer> <C-_> <Esc>^i// <Esc> " First one
" noremap <buffer> <expr>
noremap <buffer> <C-_> :s/^(?!\/\/)/^\/\/\ ./g<CR>;


" Stuff for after the plugins have run (overrides essentially)

" set filetype dependent associations
autocmd BufRead,BufNewFile *.h set filetype=c
autocmd FileType c setlocal foldmethod=manual
autocmd BufRead,BufNewFile *.s set filetype=asm
autocmd BufRead,BufNewFile *.s set syntax=asm

